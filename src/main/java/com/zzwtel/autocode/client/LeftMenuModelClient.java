package com.zzwtel.autocode.client;

import com.zzwtel.autocode.xml.LeftMenuModelCreate;

/**
 * 左菜单生成器客户端
 * @author yangtonggan
 * @date 2016-6-8
 */
public class LeftMenuModelClient {
	public static void main(String[] args){		
		LeftMenuModelCreate menu = new LeftMenuModelCreate();
		menu.create();
	}
}
