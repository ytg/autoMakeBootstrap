package com.zzwtel.autocode.client;

import com.zzwtel.autocode.xml.LayoutModelCreate;

/**
 * 布局模型生成器客户端
 * @author yangtonggan
 * @date 2016-3-11
 *
 */
public class LayoutModelClient {
	public static void main(String[] args){		
		LayoutModelCreate layout = new LayoutModelCreate();
		layout.create();
	}
}
