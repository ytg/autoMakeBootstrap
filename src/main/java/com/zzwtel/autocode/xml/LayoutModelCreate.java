package com.zzwtel.autocode.xml;

import java.util.List;

import com.zzwtel.autocode.db.DBUtil;
import com.zzwtel.autocode.template.model.Table;
import com.zzwtel.autocode.util.FileUtil;
import com.zzwtel.autocode.util.HumpUtil;
import com.zzwtel.autocode.util.PathUtil;

/**
 * 布局模型生成器
 * @author yangtonggan
 * @date 2016-3-10
 */
public class LayoutModelCreate {
	public void create(){
		//先打印xml
		StringBuilder xml = new StringBuilder();
		xml.append("<?xml version='1.0' encoding='UTF-8'?>\n");
		xml.append("<layout-model>\n");
		List<Table> tables = DBUtil.getAllTables();
		for(Table t : tables){			
			xml.append("	<layout table='"+t.getName()+"'>\n");
			xml.append("		<list-layout style='1'></list-layout>\n");
			xml.append("		<form-layout column='1'></form-layout>\n");
			xml.append("	</layout>\n");
		}		
		xml.append("</layout-model>\n");
		//输出到文件
		String root = PathUtil.getModelRoot();
		FileUtil.write(root+"layout/layout-model.xml", xml.toString());		
		System.out.println("请配置"+root+"layout/layout-model.xml文件");
	}
	
	public static void main(String[] args){
		LayoutModelCreate layout = new LayoutModelCreate();
		layout.create();
	}
}
