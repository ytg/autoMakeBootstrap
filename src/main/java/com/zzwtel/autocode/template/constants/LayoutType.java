package com.zzwtel.autocode.template.constants;

public class LayoutType {
	//表格
	public static String LIST = "list";
	//单列添加表单
	public static String SINGLE_COLUMN_ADD_FORM = "single_column_add_form";
	//单列编辑表单
	public static String SINGLE_COLUMN_EDIT_FORM = "single_column_edit_form";
	//两列添加表单
	public static String TWO_COLUMN_ADD_FORM = "two_column_add_form";
	//两列编辑表单
	public static String TWO_COLUMN_EDIT_FORM = "two_column_edit_form";
}
