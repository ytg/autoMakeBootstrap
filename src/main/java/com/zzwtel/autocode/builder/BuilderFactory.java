package com.zzwtel.autocode.builder;

import java.util.List;
import java.util.Map;

import com.zzwtel.autocode.template.constants.LayoutType;
import com.zzwtel.autocode.template.model.ControllerModel;
import com.zzwtel.autocode.template.model.TemplateModel;
import com.zzwtel.autocode.template.model.UIModel;
import com.zzwtel.autocode.template.model.UrlModel;
import com.zzwtel.autocode.template.model.ViewModelContainer;

/**
 * 代码构造工厂
 * @author yangtonggan
 * 模块路径在类中设置
 */
public class BuilderFactory {
	/**
	 * 生成代码
	 */
	public void makeCode(){
		List<UrlModel> urls = readUrl();
		UrlConstantsBuilder urlBuilder = new UrlConstantsBuilder();
		urlBuilder.makeCode(urls);
		
		//读取config
		Map<String,TemplateModel> viewCfg = readCfg();
		//填充视图容器
		ViewModelContainer container = new ViewModelContainer();
		for(Map.Entry<String,TemplateModel> entry : viewCfg.entrySet()){
			container.addView(entry.getKey(), entry.getValue());
		}	
		//迭代容器，判断视图类型，调用对应的构造器，生成代码
		for(TemplateModel tm : container.getViewMap().values()){
			if (tm instanceof ControllerModel){
				//此处生成controller类
				ControllerJavaBuilder builder = new ControllerJavaBuilder();
				builder.makeCode((ControllerModel)tm, "system");
			}
			
			if(tm instanceof UIModel){
				UIModel vm = (UIModel)tm;
				if("user".equals(vm.getTable().getName())){
					System.out.println("user user============$$$$$$$$$$$$$$$$$$$$$============");
				}
				if(LayoutType.LIST.equals(vm.getLayout())){
					//生成list html 和js文件
					ListHtmlBuilder html = new ListHtmlBuilder();
					html.makeCode(vm,"system");
					ListJsBuilder js = new ListJsBuilder();
					js.makeCode(vm,"system");
				}else if(LayoutType.SINGLE_COLUMN_ADD_FORM.equals(vm.getLayout())){
					//生成 添加表单 html和js文件	
					AddFormHtmlBuilder html = new AddFormHtmlBuilder();
					html.makeCode(vm,"system");				
					AddFormJsBuilder js = new AddFormJsBuilder();
					js.makeCode(vm,"system");				
				}else if(LayoutType.SINGLE_COLUMN_EDIT_FORM.equals(vm.getLayout())){
					//生成编辑表单html和js文件
					EditFormHtmlBuilder html = new EditFormHtmlBuilder();
					html.makeCode(vm,"system");				
					EditFormJsBuilder js = new EditFormJsBuilder();
					js.makeCode(vm,"system");
				}
				
				else if(LayoutType.TWO_COLUMN_ADD_FORM.equals(vm.getLayout())){
					//生成编辑表单html和js文件				
					AddForm2ColumnHtmlBuilder html = new AddForm2ColumnHtmlBuilder();
					html.makeCode(vm,"system");				
					AddFormJsBuilder js = new AddFormJsBuilder();
					js.makeCode(vm,"system");					
				}else if(LayoutType.TWO_COLUMN_EDIT_FORM.equals(vm.getLayout())){
					//生成编辑表单html 和js文件
					EditForm2ColumnHtmlBuilder html = new EditForm2ColumnHtmlBuilder();
					html.makeCode(vm,"system");				
					EditFormJsBuilder js = new EditFormJsBuilder();
					js.makeCode(vm,"system");
				}
				
				
			}
			
		}
		
	}

	/**
	 * 获取配置中的模型数据
	 * @return
	 */
	private Map<String, TemplateModel> readCfg() {	
		return Config.cfgModel();
	}
	
	/**
	 * 获取配置中的模型数据
	 * @return
	 */
	private List<UrlModel> readUrl() {	
		return Config.cfgUrls();
	}
	
	
}
