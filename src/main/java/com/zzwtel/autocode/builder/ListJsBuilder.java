package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.ListJsTemplate;
import com.zzwtel.autocode.template.model.UIModel;

/**
 * 列表js构造器
 * @author yangtongan
 * @date 2016-3-7
 */
public class ListJsBuilder {

	public void makeCode(UIModel vm,String modularDir) {
		ListJsTemplate template = new ListJsTemplate();
		template.generate(vm, modularDir);		
	}

}
