package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Build;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.BuildMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Build控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class BuildController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_BUILD)
	public void list(){				
		//BuildServicePrx prx = IceServiceUtil.getService(BuildServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/build_list.html");
	}
	
	/**
	 * 添加Build
	 */
	@ActionKey(UrlConstants.URL_BUILD_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Build成功");;
		try{
			//BuildServicePrx prx = IceServiceUtil.getService(BuildServicePrx.class);
			Build model = getBean(Build.class);		
			//prx.addBuild(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Build失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Build
	 */
	@ActionKey(UrlConstants.URL_BUILD_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Build成功");
		try{
			String ids = getPara("ids");		
			//BuildServicePrx prx = IceServiceUtil.getService(BuildServicePrx.class);
			//prx.delBuildByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Build失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_BUILD_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Build entity = new Build();	
		setAttr("build", entity);
		render("/system/view/build_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_BUILD_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//BuildServicePrx prx = IceServiceUtil.getService(BuildServicePrx.class);
			//BuildM modele = prx.inspectBuild(id);
			Build entity = new Build();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("build", entity);			
			render("/system/view/build_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Build失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Build
	 */
	@ActionKey(UrlConstants.URL_BUILD_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Build成功");
		try{
			//BuildServicePrx prx = IceServiceUtil.getService(BuildServicePrx.class);
			Build model = getBean(Build.class);
			//prx.alterBuild(model);
		}catch(Exception e){
			repJson = new DataObject("更新Build失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(BuildServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//BuildMPage viewModel = prx.getBuildList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Build[] aray = new Build[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Build();
			aray[i].id = "id-"+i;			
		}	
		List<Build> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
