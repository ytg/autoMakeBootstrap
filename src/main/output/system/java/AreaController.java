package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.AreaM;
import com.zzwtec.community.model.AreaMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AreaServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Area控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AreaController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_AREA)
	public void list(){				
		//AreaServicePrx prx = IceServiceUtil.getService(AreaServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/area_list.html");
	}
	
	/**
	 * 添加Area
	 */
	@ActionKey(UrlConstants.URL_AREA_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Area成功");;
		try{
			//AreaServicePrx prx = IceServiceUtil.getService(AreaServicePrx.class);
			Area model = getBean(Area.class);		
			//prx.addArea(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Area失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Area
	 */
	@ActionKey(UrlConstants.URL_AREA_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Area成功");
		try{
			String ids = getPara("ids");		
			//AreaServicePrx prx = IceServiceUtil.getService(AreaServicePrx.class);
			//prx.delAreaByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Area失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_AREA_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Area entity = new Area();	
		setAttr("area", entity);
		render("/system/view/area_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_AREA_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AreaServicePrx prx = IceServiceUtil.getService(AreaServicePrx.class);
			//AreaM modele = prx.inspectArea(id);
			Area entity = new Area();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("area", entity);			
			render("/system/view/area_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Area失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Area
	 */
	@ActionKey(UrlConstants.URL_AREA_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Area成功");
		try{
			//AreaServicePrx prx = IceServiceUtil.getService(AreaServicePrx.class);
			Area model = getBean(Area.class);
			//prx.alterArea(model);
		}catch(Exception e){
			repJson = new DataObject("更新Area失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AreaServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AreaMPage viewModel = prx.getAreaList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Area[] aray = new Area[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Area();
			aray[i].id = "id-"+i;			
		}	
		List<Area> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
